$(function() {

console.log("content.js start");

var url = window.location.href;
var handle = getHandle({"url": url});

function requestListener(request, sender, sendResponse) {
	console.debug("%s", JSON.stringify(request));
	var resp = { 
		"action": request.action,
		"result": 0
	};
}

function init() {
	chrome.extension.onRequest.addListener(requestListener);
	
	if (handle) {
		if ($.isFunction(handle["revise"])) {
			console.debug("revise");
			handle.revise({"url": url});
		}	
	}
}

init();

});

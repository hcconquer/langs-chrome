function strfmt(str, len, right) {
	var left = len - str.length - right;
	for (var i = 0; i < left; i++) {
		str = " " + str;
	}
	for (var i = 0; i < right; i++) {
		str = str + " ";
	}
	return str;
}

function getDomainByUrl(url) {
	var match = url.match(/:\/\/(.[^/]+)/);
	if (typeof match == 'undefined' || match == null) {
		return null;
	}
	return match[1];
}

function getMainDomainByDomain(domain) {
	var ds = domain.split('.');
	var num = ds.length;
	if (num >= 3) {
		if (ds[num - 2] == 'com') {
			return ds[num - 3] + '.' + ds[num - 2] + '.' + ds[num - 1];
		} else {
			return ds[num - 2] + '.' + ds[num - 1];
		}
	} else {
		return domain;
	}
}

function getMainDomainByUrl(url) {
	var domain = getDomainByUrl(url);
	return getMainDomainByDomain(domain);
}

function getProtocolByUrl(url) {
	var idx = url.indexOf(':');
	if (idx < 0) {
		return null;
	}
	return url.substring(0, idx);
}

function addScript(script) {
	$('<script></script>').attr({type: 'text/javascript'})
	.text(script).appendTo($('body'));
}

function getUnixTime(type) {
	var msec = new Date().getTime();
	if (type == "msec") {
		return msec;
	}
	return Math.round(msec / 1000);
}

function delay(func, msec, times) {
	var begin = getUnixTime("msec");
	var rts = 0;
	var iv = setInterval(function() {
		var now = getUnixTime("msec");
		if (now - begin >= msec) {
			// console.debug("%s %s %s", now, begin, msec);
			rts++;
			if ((times > 0) && (rts >= times)) {
				clearInterval(iv);
			} else {
				begin = getUnixTime("msec");
			}
			var ret = func();
			if (ret == true) {
				console.debug("delay exit, rts: %d, max: %d", rts, times);
				clearInterval(iv);
			}
		}
	}, 1);
}

function showOptionsPage(tab) {
	var manifest = chrome.runtime.getManifest();
	var url = chrome.runtime.getURL(manifest.options_page);
	if (tab) {
		chrome.tabs.update(tab.id, {
			"url": url
		});
	} else {
		chrome.tabs.create(
			{
				"url": url
			}, 
			function(ntab) {
			}
		);
	}
}

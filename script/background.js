(function(window) {

console.log("background.js start");

var CTXT_MENU_OPEN_SHANBAY = "ctxt_menu_open_shanbay";
var CTXT_MENU_OPEN_YOUDAO = "ctxt_menu_open_youdao";
var CTXT_MENU_OPEN_ICIBA = "ctxt_menu_open_iciba";

function initCtxt(params) {
	if ($.isPlainObject(params)) {
		params.title = chrome.i18n.getMessage(params.title);
		chrome.contextMenus.create(params);
	} else if ($.isArray(params)) {
		for (var i = 0; i < params.length; i++) {
			initCtxt(params[i]);
		}
	}
}

function setCtxt(params) {
}

function openPage(params) {
	console.debug("%s", JSON.stringify(params));
	chrome.tabs.query({
		"url": params.wildcard
	}, function(tabs) {
		console.debug("match, %s", JSON.stringify(tabs));
		if (tabs.length > 0) { // opened
			var dtab = tabs[0];
			chrome.tabs.query({
				"active": true
			}, function(tabs) {
				console.debug("active, %s", JSON.stringify(tabs));
				var curtab = tabs[0];
				if (curtab.id == dtab.id) {
				} else {
					chrome.windows.update(dtab.windowId, {
						"focused": true,
					}, function() {
						chrome.tabs.update(dtab.id, {
							"active": true
						}, function(umctab) {
						});
					});
				}
			});
		} else { // tab not open
			chrome.tabs.create({
				"url": params.url
			}, function(tab) {
			});
		}
	});
}

function reloadPage(params) {
	var url = "<all_urls>";
	if ((params) && (params.url)) {
		url = params.url;
	}
	console.debug("url: %s", url);
	chrome.tabs.query({
		"url": url
	}, function(tabs) {
		for (var i = 0; i < tabs.length; i++) {
			var tab = tabs[i];
			chrome.tabs.reload(tab.id, {}, function() {
			});	
		}
	});
}

function pageOpenListener(tab) {
}

/*
 * when page is all loaded include css and images
 */
function pageReadyListener(tab) {
	// console.debug(tab.url);
}

function tabCreatedListener(tab) {
	// console.debug(tab.url);
}

function tabUpdatedListener(tabId, event, tab) {
	// console.debug("%s %s %s", tabId, JSON.stringify(event), JSON.stringify(tab));
	if (event.status) {
		if (event.status == 'loading') {
			pageOpenListener(tab);
		} else if (event.status == 'complete') {
			pageReadyListener(tab);
		}
	}
}

function tabRemovedListener(tabId, info) {
}

function beforeRequestListener(details) {
	console.debug(JSON.stringify(details));
	var url = details.url;
	var handle = getHandle({"url": url});
	if ((handle) && ($.isFunction(handle.filter))) {
		if (!handle.filter(url)) {
			return {"cancel": true};
		}
	}
}

function beforeSendHeadersListener(details) {
	var url = details.url;
	var handle = getHandle({"url": url});
	if ((handle) && ($.isFunction(handle.referer))) {
		var ref = handle.referer({"url": url});
		if (!ref) {
			console.debug("remove referer, url: %s", url);
			for (var i = 0; i < details.requestHeaders.length; i++) {
				if (details.requestHeaders[i].name === "Referer") {
					details.requestHeaders.splice(i, 1);
					break;
				}
			}		
		}
	}
	return {"requestHeaders": details.requestHeaders};
}

function completedRequestListener(details) {
}

function popupClickedListener(tab) {
}

/*
 * get request from page or popup
 */
function extensionRequestListener(request, sender, sendResponse) {
	console.debug(JSON.stringify(request));
	var resp = { 
		"action": request.action,
		"result": 0
	};
}

function onInstalledListener(info) {
	console.debug(JSON.stringify(info));
	// extension install or update
	if (info.reason == 'install'){
		saveOptions(null); 
		// showOptionsPage(null);
	} else if (info.reason == 'update') {
		// showOptionsPage(null);
	}
}

function init() {
	var manifest = chrome.runtime.getManifest();
	console.debug(JSON.stringify(manifest));
	
	var urls = getHandle({"wildcards": true});
	console.debug("watch, urls: %s", JSON.stringify(urls));
	
	chrome.webRequest.onBeforeRequest.addListener(
			beforeRequestListener, 
			{ urls: urls },
			[ "blocking", "requestBody" ]
	);
	
	chrome.webRequest.onBeforeSendHeaders.addListener(
			beforeSendHeadersListener, 
			{ urls: urls },
			[ "blocking", "requestHeaders" ]
	);
	
	chrome.webRequest.onCompleted.addListener(
			completedRequestListener,
			{ urls: urls }
	);
	
	chrome.tabs.onCreated.addListener(tabCreatedListener);
	chrome.tabs.onUpdated.addListener(tabUpdatedListener);
	chrome.tabs.onRemoved.addListener(tabRemovedListener);
	
	chrome.browserAction.onClicked.addListener(popupClickedListener);
	chrome.extension.onRequest.addListener(extensionRequestListener);
	chrome.runtime.onInstalled.addListener(onInstalledListener);
	
	initCtxt([
	    {
			"id": CTXT_MENU_OPEN_SHANBAY,
			"title": CTXT_MENU_OPEN_SHANBAY,
			"contexts": [ "browser_action" ],
			"onclick": function() {
				console.debug("%s", CTXT_MENU_OPEN_SHANBAY);
				openPage({
					"wildcard": "*://*.shanbay.com/*",
					"url": "http://www.shanbay.com/"
				});
			}
	    },
	    {
			"id": CTXT_MENU_OPEN_YOUDAO,
			"title": CTXT_MENU_OPEN_YOUDAO,
			"contexts": [ "browser_action" ],
			"onclick": function() {
				console.debug("%s", CTXT_MENU_OPEN_YOUDAO);
				openPage({
					"wildcard": "*://*.youdao.com/*",
					"url": "http://dict.youdao.com/"
				});
			}
	    },
	    {
			"id": CTXT_MENU_OPEN_ICIBA,
			"title": CTXT_MENU_OPEN_ICIBA,
			"contexts": [ "browser_action" ],
			"onclick": function() {
				console.debug("%s", CTXT_MENU_OPEN_ICIBA);
				openPage({
					"wildcard": "*://*.iciba.com/*",
					"url": "http://www.iciba.com/"
				});
			}
	    }
	]);
}

init();

})(window);

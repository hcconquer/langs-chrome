function getHandle(params) {
	var handles = [
      	{
       		"regex": ".*://.*shanbay\.com/.*/(library)/.*\?.*",
       		"wildcard": "*://*.shanbay.com/*",
       		"revise": function(params) {
       		},
       	},
       	{ 
       		"regex": ".*://.*shanbay\.com/.*/(learning|review)/.*",
       		"wildcard": "*://*.shanbay.com/*",
       		"revise": function(params) {
       			delay(function() {
       				var jword = $("#current-learning-word");
       				var word = jword.text();
       				if (!word) {
       					return;
       				}
       				if (jword.attr("word") == word) {
       					// console.debug("been revised");
       					return;
       				}
       				/*
       				if (jword.attr("revised")) {
       					return true;
       				}
       				*/
       				var jimg = $("#cliparts div > a > img");
       				if (jimg.length <= 0) {
       					return;
       				}
   					// jword.attr("revised", false);
   	       			getWordImageList({
   	       				"word": word,
   	       				"max": -1,
   	       				"success": function(params) {
   	       					if ((params) && (params.images)) {
   	       						if (params.images.length > 0) {
   	       							var ridx = Math.floor(Math.random() * params.images.length);
   	       							jimg.attr("src", params.images[ridx]);
   	       							console.debug("update image, word: %s, idx:%s, src: %s", word, ridx, params.images[ridx]);
   	       						} else {
   	       							console.debug("no images, word: %s", word);
   	       						}
   	       						jword.attr("word", word);
   	       					}
   	       				}
   	       			});	
       			}, 200, -1);
       		},
       	},
       	{
       		"regex": ".*://.*(youdao|ydstatic)\.com/image.*",
       		"wildcard": "*://*.com/image*",
       		"referer": function(params) {
       			return false;
       		}
       	},
       	{
       		"regex": ".*//dict\.youdao\.com/.*",
       		"wildcard": "*://dict.youdao.com/*",
       		"revise": function(params) {
       			delay(function() {
       				$(document).unbind("keydown").keydown(function(event) {
       					console.debug("keydown, key: %s", JSON.stringify(event.keyCode));
       					var charCode = String.fromCharCode(event.keyCode);
       					if ((charCode == 'P') && (event.ctrlKey)) {
           					$("span.pronounce > a").get(-1).click();
       					} else if ((charCode == 'S') && (event.ctrlKey)) {
       						// $("input.s-btn").get(-1).click();
       					}
       				});
       			}, 200, 1);
       		}
       	}
    ];
	if (params.url) {
		for (var i = 0; i < handles.length; i++) {
			var handle = handles[i];
			if (params.url.match(handle.regex)) {
				return handle;
			}
		}
		return null;
	}
	if (params.wildcards) {
		var urls = [];
		for (var i = 0; i < handles.length; i++) {
			var handle = handles[i];
			if (handle.wildcard) {
				var has = false;
				for (var j = 0; j < urls.length; j++) {
					var url = urls[j];
					if (url == handle.wildcard) {
						has = true;
						break;
					}
				}
				if (!has) {
					urls.push(handle.wildcard);
				}
			}
		}
		return urls;
	}
	return null;
}

function getShanBayWord(params) {
	var wordid = null, url = null;
	if ((params) && (params.wordid)) {
		wordid = params.wordid;
		url = "http://www.shanbay.com/api/v1/bdc/learning/?basic=0&ids=" + wordid;
	}
	if (!url) {
		return;
	}
	$.ajax(url, {
		"type": "GET",
		"success": function(data, textStatus, jqXHR) {
			var word = data.data[0].content;
			if ($.isFunction(params.success)) {
				params.success({"word": word});
			}
		}
	});
}

function getWordImageList(params) {
	var word = null, url = null, max = -1;
	if ((params) && (params["word"])) {
		word = params.word;
		url = "https://dict.youdao.com/ugc/wordjson/" + word;
	}
	if ((params) && (params.max)) {
		max = params.max;
	}
	if (!url) {
		return;
	}
	$.ajax(url, {
		"type": "GET",
		"success": function(data, textStatus, jqXHR) {
			var images = data;
			var urls = new Array();
			for (var i = 0; i < images.length; i++) {
				console.debug(images[i]);
				urls.push(images[i]["Url"]);
				if ((max >= 0) && (images.length >= max)) {
					break;
				}
			}
			if ($.isFunction(params["success"])) {
				params.success({
					"images": urls
				});
			}
		}
	});
}